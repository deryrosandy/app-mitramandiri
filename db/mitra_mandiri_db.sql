/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : mitra_mandiri_db

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 16/10/2021 12:42:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for aauth_login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `aauth_login_attempts`;
CREATE TABLE `aauth_login_attempts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(39) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `timestamp` datetime(0) NULL DEFAULT NULL,
  `login_attempts` tinyint(2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aauth_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_pms
-- ----------------------------
DROP TABLE IF EXISTS `aauth_pms`;
CREATE TABLE `aauth_pms`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_sent` datetime(0) NULL DEFAULT NULL,
  `date_read` datetime(0) NULL DEFAULT NULL,
  `pm_deleted_sender` int(1) NOT NULL,
  `pm_deleted_receiver` int(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `full_index`(`id`, `sender_id`, `receiver_id`, `date_read`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aauth_pms
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_user_variables
-- ----------------------------
DROP TABLE IF EXISTS `aauth_user_variables`;
CREATE TABLE `aauth_user_variables`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aauth_user_variables
-- ----------------------------

-- ----------------------------
-- Table structure for aauth_users
-- ----------------------------
DROP TABLE IF EXISTS `aauth_users`;
CREATE TABLE `aauth_users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pass` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `banned` tinyint(1) NULL DEFAULT 0,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `last_activity` datetime(0) NULL DEFAULT NULL,
  `date_created` datetime(0) NULL DEFAULT NULL,
  `forgot_exp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `remember_time` datetime(0) NULL DEFAULT NULL,
  `remember_exp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `verification_code` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `totp_secret` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip_address` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `roleid` int(1) NOT NULL,
  `picture` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of aauth_users
-- ----------------------------
INSERT INTO `aauth_users` VALUES (14, 'deryrosandy@gmail.com', '2a74b4a28f00408c278c6620a81f507dd1a74a7e0237eab83c4a967665968f48', 'superadmin', 0, '2021-10-16 12:35:28', '2021-10-16 12:35:28', '2021-10-12 18:00:03', NULL, NULL, NULL, '', NULL, '::1', 5, 'example.png');
INSERT INTO `aauth_users` VALUES (15, 'admin@admin.com', 'e9fbdb0119967e2d41a0830400bc09461a2789b510cb6d4f2ce4e3eecd2c68c7', 'admin', 0, '2021-10-15 20:15:46', '2021-10-15 20:15:46', '2021-10-15 20:14:12', NULL, NULL, NULL, NULL, NULL, '::1', 3, 'example.png');

-- ----------------------------
-- Table structure for accounts
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `acn` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `holder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `adate` datetime(0) NOT NULL,
  `lastbal` decimal(16, 2) NULL DEFAULT 0.00,
  `code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `acn`(`acn`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES (1, '12345678', 'MANDIRI', '2018-01-01 00:00:00', 0.00, '');
INSERT INTO `accounts` VALUES (3, '123456789', 'BRI', '0000-00-00 00:00:00', 0.00, '');
INSERT INTO `accounts` VALUES (4, '11122334455', 'KAS KECIL', '0000-00-00 00:00:00', 0.00, '');
INSERT INTO `accounts` VALUES (5, '22334455', 'KAS BESAR', '0000-00-00 00:00:00', 8400000.00, '');

-- ----------------------------
-- Table structure for api_keys
-- ----------------------------
DROP TABLE IF EXISTS `api_keys`;
CREATE TABLE `api_keys`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of api_keys
-- ----------------------------
INSERT INTO `api_keys` VALUES (1, 0, '552a136dfc53cf4f45d951db', 0, 0, 0, NULL, '2021-10-15');

-- ----------------------------
-- Table structure for app_system
-- ----------------------------
DROP TABLE IF EXISTS `app_system`;
CREATE TABLE `app_system`  (
  `id` int(1) NOT NULL,
  `cname` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `region` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `country` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `postbox` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `taxid` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tax` int(11) NOT NULL,
  `currency` varchar(4) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `currency_format` int(1) NOT NULL,
  `prefix` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dformat` int(1) NOT NULL,
  `zone` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `logo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lang` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'english',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of app_system
-- ----------------------------
INSERT INTO `app_system` VALUES (1, 'Mitra Mandiri', 'Jl. Raya Cikuda Wanaherang Cilengsi', 'Bogor', 'Jawa Barat', 'ID', '16965', '0813-9818-1628', 'support@khanzasoft.com', '1234', 0, 'Rp. ', 0, 'SRN', 1, 'Asia/Jakarta', '16343625441748191601.png', 'indonesian');

-- ----------------------------
-- Table structure for bank_accounts
-- ----------------------------
DROP TABLE IF EXISTS `bank_accounts`;
CREATE TABLE `bank_accounts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bank` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `acn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `branch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enable` enum('Yes','No') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bank_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for billing_terms
-- ----------------------------
DROP TABLE IF EXISTS `billing_terms`;
CREATE TABLE `billing_terms`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(1) NOT NULL,
  `terms` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of billing_terms
-- ----------------------------
INSERT INTO `billing_terms` VALUES (1, 'Payment on receipt', 0, 'Payment due on receipt');

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ci_sessions_timestamp`(`timestamp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('91bc42j0k2u52vv0m2grf0mt0b8d824v', '::1', 1634359487, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335393438373B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('9k6k25a3fc8cpft20uplagukhsdo10nd', '::1', 1634359116, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335393131363B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('apc26d8su6nn0ao6bdvjr5630bvvtppl', '::1', 1634360505, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343336303530353B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('drbjmr3l8jbdmqtu8ced6j35uh1ru1bm', '::1', 1634357725, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335373732353B);
INSERT INTO `ci_sessions` VALUES ('ee2ffo00i35fvq49qb76lmos6nftn9tr', '::1', 1634362101, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343336323130313B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('ifbu6pqvnbotlniaf5vv45pfv599cor5', '::1', 1634360904, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343336303930343B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('j7k9b27qaicceu8s9uagus7hl7pg9qap', '::1', 1634358073, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335383037333B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('lhdschurbgbdfrff7ouurh96ara4ltuf', '::1', 1634359802, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335393830323B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('mf7cdeg4ffk51v24humfbgjar0voms3s', '::1', 1634362554, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343336323535343B);
INSERT INTO `ci_sessions` VALUES ('opnhg7bi60ukns0ta55q6k2o9rcr7vj4', '::1', 1634358495, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335383439353B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('osakrd184mha2etbf9s5pg7qing43hsm', '::1', 1634361772, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343336313737323B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('v395qmc7ogdrndecs0a4ishjpeb94uc1', '::1', 1634358807, 0x5F5F63695F6C6173745F726567656E65726174657C693A313633343335383830373B69647C733A323A223134223B757365726E616D657C733A31303A22737570657261646D696E223B656D61696C7C733A32313A2264657279726F73616E647940676D61696C2E636F6D223B6C6F67676564696E7C623A313B);

-- ----------------------------
-- Table structure for conf
-- ----------------------------
DROP TABLE IF EXISTS `conf`;
CREATE TABLE `conf`  (
  `id` int(1) NOT NULL DEFAULT 1,
  `bank` int(1) NOT NULL,
  `acid` int(11) NOT NULL,
  `ext1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ext2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `recaptcha_p` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `captcha` int(1) NOT NULL,
  `recaptcha_s` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  UNIQUE INDEX `id_2`(`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of conf
-- ----------------------------
INSERT INTO `conf` VALUES (1, 1, 1, 'ltr', '0', '0', 0, '0');

-- ----------------------------
-- Table structure for corn_job
-- ----------------------------
DROP TABLE IF EXISTS `corn_job`;
CREATE TABLE `corn_job`  (
  `id` int(1) NOT NULL,
  `cornkey` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `rec_email` int(11) NULL DEFAULT NULL,
  `email` int(11) NULL DEFAULT NULL,
  `rec_due` int(11) NULL DEFAULT NULL,
  `recemail` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of corn_job
-- ----------------------------
INSERT INTO `corn_job` VALUES (1, '345648', 0, 0, 0, '0');

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `symbol` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rate` decimal(10, 3) NOT NULL,
  `thous` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dpoint` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `decim` int(2) NOT NULL,
  `cpos` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES (1, 'GBP', 'X', 0.717, ',', '.', 2, 1);

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `postbox` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `picture` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'example.png',
  `gid` int(5) NOT NULL DEFAULT 1,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taxid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ID',
  `postbox_s` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `balance` float(16, 2) NULL DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `gid`(`gid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'PT. MERDEKA', '09776767', 'Jakarta', '', '', '', '', 'merdeka@gmail.com', 'example.png', 1, '', '', 'PT. MERDEKA', '09776767', 'merdeka@gmail.com', 'Jakarta', '', '', '', '', 0.00);

-- ----------------------------
-- Table structure for customers_group
-- ----------------------------
DROP TABLE IF EXISTS `customers_group`;
CREATE TABLE `customers_group`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `summary` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of customers_group
-- ----------------------------
INSERT INTO `customers_group` VALUES (1, 'Default Group', 'Default Group');

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `filename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cdate` date NOT NULL,
  `permission` int(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of documents
-- ----------------------------

-- ----------------------------
-- Table structure for employee_profile
-- ----------------------------
DROP TABLE IF EXISTS `employee_profile`;
CREATE TABLE `employee_profile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ID',
  `postbox` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phonealt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `picture` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'example.png',
  `sign` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'sign.png',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employee_profile
-- ----------------------------
INSERT INTO `employee_profile` VALUES (14, 'admin', 'BusinessOwner', 'Test Street', 'Test City', 'Test Region', 'ID', '123456', '12345678', '0', 'example.png', 'sign.png');
INSERT INTO `employee_profile` VALUES (15, 'admin', 'Admin', 'Jakarta', 'Jakarta', 'Jakarta', 'ID', '123', '0', NULL, 'example.png', 'sign.png');

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `color` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '#3a87ad',
  `start` datetime(0) NOT NULL,
  `end` datetime(0) NULL DEFAULT NULL,
  `allDay` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'true',
  `rel` int(2) NOT NULL DEFAULT 0,
  `rid` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rel`(`rel`) USING BTREE,
  INDEX `rid`(`rid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of events
-- ----------------------------

-- ----------------------------
-- Table structure for goals
-- ----------------------------
DROP TABLE IF EXISTS `goals`;
CREATE TABLE `goals`  (
  `id` int(1) NOT NULL,
  `income` bigint(20) NOT NULL,
  `expense` bigint(20) NOT NULL,
  `sales` bigint(20) NOT NULL,
  `netincome` bigint(20) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goals
-- ----------------------------
INSERT INTO `goals` VALUES (1, 100000000, 25000000, 100000, 100000);

-- ----------------------------
-- Table structure for invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE `invoice_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT 0,
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `price` decimal(16, 2) NOT NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `totaltax` decimal(16, 2) NULL DEFAULT 0.00,
  `totaldiscount` decimal(16, 2) NULL DEFAULT 0.00,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `invoice`(`tid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of invoice_items
-- ----------------------------
INSERT INTO `invoice_items` VALUES (4, 1001, 1, 'KERAMIK MULIA UKURAN 30', 120, 70000.00, 0.00, 0.00, 8400000.00, 0.00, 0.00, 'sasaa');

-- ----------------------------
-- Table structure for invoices
-- ----------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `invoiceduedate` date NOT NULL,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `shipping` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `total` decimal(16, 2) NULL DEFAULT 0.00,
  `pmethod` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('paid','due','canceled','partial') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'due',
  `csd` int(5) NOT NULL DEFAULT 0,
  `eid` int(4) NOT NULL,
  `pamnt` decimal(16, 2) NULL DEFAULT 0.00,
  `items` int(11) NOT NULL,
  `taxstatus` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'yes',
  `discstatus` tinyint(1) NOT NULL,
  `format_discount` enum('%','flat','b_p','bflat') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '%',
  `refer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `term` int(3) NOT NULL,
  `multi` int(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `invoice`(`tid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `csd`(`csd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of invoices
-- ----------------------------
INSERT INTO `invoices` VALUES (3, 1001, '2021-10-16', '2021-10-18', 8400000.00, 0.00, 0.00, 0.00, 8400000.00, 'Cash', '', 'paid', 1, 14, 8400000.00, 120, '', 1, '%', '123', 1, NULL);

-- ----------------------------
-- Table structure for meta_data
-- ----------------------------
DROP TABLE IF EXISTS `meta_data`;
CREATE TABLE `meta_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(3) NOT NULL,
  `rid` int(11) NOT NULL,
  `col1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `col2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE,
  INDEX `rid`(`rid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of meta_data
-- ----------------------------

-- ----------------------------
-- Table structure for milestones
-- ----------------------------
DROP TABLE IF EXISTS `milestones`;
CREATE TABLE `milestones`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `exp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `color` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of milestones
-- ----------------------------

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cdate` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for online_payment
-- ----------------------------
DROP TABLE IF EXISTS `online_payment`;
CREATE TABLE `online_payment`  (
  `id` int(11) NOT NULL,
  `default_acid` int(11) NOT NULL DEFAULT 1,
  `currency_code` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `enable` int(1) NOT NULL,
  `bank` int(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of online_payment
-- ----------------------------
INSERT INTO `online_payment` VALUES (1, 1, 'USD', 1, 1);

-- ----------------------------
-- Table structure for payment_gateways
-- ----------------------------
DROP TABLE IF EXISTS `payment_gateways`;
CREATE TABLE `payment_gateways`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `enable` enum('Yes','No') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `key1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `key2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `currency` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'USD',
  `dev_mode` enum('true','false') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ord` int(5) NOT NULL,
  `surcharge` decimal(16, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of payment_gateways
-- ----------------------------
INSERT INTO `payment_gateways` VALUES (1, 'Stripe', 'Yes', 'sk_test_secratekey', 'sk_test_publickey', 'USD', 'true', 1, 0.00);
INSERT INTO `payment_gateways` VALUES (2, 'Authorize.Net', 'Yes', 'TRANSACTIONKEY', 'LOGINID', 'AUD', 'true', 2, 0.00);
INSERT INTO `payment_gateways` VALUES (3, 'Pin Payments', 'Yes', 'TEST', 'none', 'AUD', 'true', 3, 0.00);
INSERT INTO `payment_gateways` VALUES (4, 'PayPal', 'Yes', 'MyPayPalClientId', 'MyPayPalSecret', 'USD', 'true', 4, 0.00);
INSERT INTO `payment_gateways` VALUES (5, 'SecurePay', 'Yes', 'ABC0001', 'abc123', 'AUD', 'true', 5, 0.00);

-- ----------------------------
-- Table structure for product_cat
-- ----------------------------
DROP TABLE IF EXISTS `product_cat`;
CREATE TABLE `product_cat`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of product_cat
-- ----------------------------
INSERT INTO `product_cat` VALUES (2, 'KERAMIK UKURAN 40 x 40', '');
INSERT INTO `product_cat` VALUES (3, 'KERAMIK UKURAN 30', '');
INSERT INTO `product_cat` VALUES (4, 'SEMEN', '');
INSERT INTO `product_cat` VALUES (5, 'GRANIT JUMBO', '');
INSERT INTO `product_cat` VALUES (6, 'CAT PARAGON', '');
INSERT INTO `product_cat` VALUES (7, 'CAT NIPPON', '');

-- ----------------------------
-- Table structure for product_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `product_warehouse`;
CREATE TABLE `product_warehouse`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of product_warehouse
-- ----------------------------
INSERT INTO `product_warehouse` VALUES (1, 'Gudang 1', '');
INSERT INTO `product_warehouse` VALUES (2, 'Gudang 2', '');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pcat` int(3) NOT NULL DEFAULT 1,
  `warehouse` int(11) NOT NULL DEFAULT 1,
  `product_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_price` decimal(16, 2) NULL DEFAULT 0.00,
  `fproduct_price` decimal(16, 2) NOT NULL DEFAULT 0.00,
  `taxrate` decimal(16, 2) NOT NULL DEFAULT 0.00,
  `disrate` decimal(16, 2) NOT NULL DEFAULT 0.00,
  `qty` int(11) NOT NULL,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `alert` int(11) NOT NULL,
  PRIMARY KEY (`pid`) USING BTREE,
  INDEX `pcat`(`pcat`) USING BTREE,
  INDEX `warehouse`(`warehouse`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 3, 1, 'KERAMIK MULIA UKURAN 30', 'KRM2392u9', 70000.00, 100000.00, 0.00, 0.00, 120, 'sasaa', 10);

-- ----------------------------
-- Table structure for project_meta
-- ----------------------------
DROP TABLE IF EXISTS `project_meta`;
CREATE TABLE `project_meta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `meta_key` int(11) NOT NULL,
  `meta_data` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key3` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  INDEX `meta_key`(`meta_key`) USING BTREE,
  INDEX `key3`(`key3`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of project_meta
-- ----------------------------

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('Waiting','Pending','Terminated','Finished','Progress') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Pending',
  `priority` enum('Low','Medium','High','Urgent') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Medium',
  `progress` int(3) NOT NULL,
  `cid` int(11) NOT NULL,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phase` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `worth` decimal(16, 2) NOT NULL DEFAULT 0.00,
  `ptype` int(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `p_id`(`p_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of projects
-- ----------------------------

-- ----------------------------
-- Table structure for purchase
-- ----------------------------
DROP TABLE IF EXISTS `purchase`;
CREATE TABLE `purchase`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `invoiceduedate` date NOT NULL,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `shipping` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `total` decimal(16, 2) NULL DEFAULT 0.00,
  `pmethod` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('paid','due','canceled','partial') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'due',
  `csd` int(5) NOT NULL DEFAULT 0,
  `eid` int(4) NOT NULL,
  `pamnt` decimal(16, 2) NULL DEFAULT 0.00,
  `items` int(11) NOT NULL,
  `taxstatus` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'yes',
  `discstatus` tinyint(1) NOT NULL,
  `format_discount` enum('%','flat','b_p','bflat') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `refer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `term` int(3) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `invoice`(`tid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `csd`(`csd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of purchase
-- ----------------------------

-- ----------------------------
-- Table structure for purchase_items
-- ----------------------------
DROP TABLE IF EXISTS `purchase_items`;
CREATE TABLE `purchase_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `totaltax` decimal(16, 2) NULL DEFAULT 0.00,
  `totaldiscount` decimal(16, 2) NULL DEFAULT 0.00,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `invoice`(`tid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of purchase_items
-- ----------------------------

-- ----------------------------
-- Table structure for quotes
-- ----------------------------
DROP TABLE IF EXISTS `quotes`;
CREATE TABLE `quotes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `invoiceduedate` date NOT NULL,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `shipping` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `total` decimal(16, 2) NULL DEFAULT 0.00,
  `pmethod` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('pending','accepted','rejected') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'pending',
  `csd` int(5) NOT NULL DEFAULT 0,
  `eid` int(4) NOT NULL,
  `pamnt` decimal(16, 2) NOT NULL,
  `items` int(11) NOT NULL,
  `taxstatus` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'yes',
  `discstatus` tinyint(1) NOT NULL,
  `format_discount` enum('%','flat','b_p','bflat') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '%',
  `refer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `term` int(3) NOT NULL,
  `proposal` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `multi` int(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `invoice`(`tid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `csd`(`csd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of quotes
-- ----------------------------

-- ----------------------------
-- Table structure for quotes_items
-- ----------------------------
DROP TABLE IF EXISTS `quotes_items`;
CREATE TABLE `quotes_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `totaltax` decimal(16, 2) NULL DEFAULT 0.00,
  `totaldiscount` decimal(16, 2) NULL DEFAULT 0.00,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `invoice`(`tid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of quotes_items
-- ----------------------------

-- ----------------------------
-- Table structure for rec_invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `rec_invoice_items`;
CREATE TABLE `rec_invoice_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `totaltax` decimal(16, 2) NULL DEFAULT 0.00,
  `totaldiscount` decimal(16, 2) NULL DEFAULT 0.00,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `invoice`(`tid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rec_invoice_items
-- ----------------------------

-- ----------------------------
-- Table structure for rec_invoices
-- ----------------------------
DROP TABLE IF EXISTS `rec_invoices`;
CREATE TABLE `rec_invoices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `invoicedate` date NOT NULL,
  `invoiceduedate` date NOT NULL,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `shipping` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `total` decimal(16, 2) NULL DEFAULT 0.00,
  `pmethod` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('paid','due','canceled','partial') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'due',
  `csd` int(5) NOT NULL DEFAULT 0,
  `eid` int(4) NOT NULL,
  `pamnt` decimal(16, 2) NULL DEFAULT 0.00,
  `items` int(11) NOT NULL,
  `taxstatus` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'yes',
  `discstatus` tinyint(1) NULL DEFAULT NULL,
  `format_discount` enum('%','flat','b_p','bflat') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '%',
  `refer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `term` int(3) NOT NULL,
  `rec` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ron` enum('Recurring','Stopped') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Recurring',
  `multi` int(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `invoice`(`tid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `csd`(`csd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rec_invoices
-- ----------------------------

-- ----------------------------
-- Table structure for reports
-- ----------------------------
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `year` int(4) NOT NULL,
  `invoices` int(11) NOT NULL,
  `sales` decimal(16, 2) NULL DEFAULT 0.00,
  `items` int(11) NOT NULL,
  `income` decimal(16, 2) NULL DEFAULT 0.00,
  `expense` decimal(16, 2) NULL DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of reports
-- ----------------------------

-- ----------------------------
-- Table structure for stock_return
-- ----------------------------
DROP TABLE IF EXISTS `stock_return`;
CREATE TABLE `stock_return`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(8) NOT NULL,
  `invoicedate` date NOT NULL,
  `invoiceduedate` date NOT NULL,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `shipping` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `total` decimal(16, 2) NULL DEFAULT 0.00,
  `pmethod` varchar(14) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('pending','accepted','rejected','partial','canceled') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'pending',
  `csd` int(5) NOT NULL DEFAULT 0,
  `eid` int(4) NOT NULL,
  `pamnt` decimal(16, 2) NULL DEFAULT 0.00,
  `items` int(11) NOT NULL,
  `taxstatus` enum('yes','no') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'yes',
  `discstatus` tinyint(1) NOT NULL,
  `format_discount` enum('%','flat','b_p','bflat') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `refer` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `term` int(3) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `invoice`(`tid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE,
  INDEX `csd`(`csd`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_return
-- ----------------------------

-- ----------------------------
-- Table structure for stock_return_items
-- ----------------------------
DROP TABLE IF EXISTS `stock_return_items`;
CREATE TABLE `stock_return_items`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `product` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(16, 2) NULL DEFAULT 0.00,
  `tax` decimal(16, 2) NULL DEFAULT 0.00,
  `discount` decimal(16, 2) NULL DEFAULT 0.00,
  `subtotal` decimal(16, 2) NULL DEFAULT 0.00,
  `totaltax` decimal(16, 2) NULL DEFAULT 0.00,
  `totaldiscount` decimal(16, 2) NULL DEFAULT 0.00,
  `product_des` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `invoice`(`tid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of stock_return_items
-- ----------------------------

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `region` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ID',
  `postbox` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `picture` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'example.png',
  `gid` int(5) NOT NULL DEFAULT 1,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `taxid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES (1, 'AYU PARAGON', '0988677', 'jakarta', 'jakarta', 'jakarta', 'jakarta', '', 'ayu.catparagon@gmail.com', 'example.png', 1, 'PARAGON', '');

-- ----------------------------
-- Table structure for sys_smtp
-- ----------------------------
DROP TABLE IF EXISTS `sys_smtp`;
CREATE TABLE `sys_smtp`  (
  `id` int(11) NOT NULL,
  `host` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `port` int(11) NOT NULL,
  `auth` enum('true','false') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `auth_type` enum('none','tls','ssl') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sender` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_smtp
-- ----------------------------
INSERT INTO `sys_smtp` VALUES (1, 'smtp.com', 587, 'true', 'none', 'support@example.com', '123456', 'support@example.com');

-- ----------------------------
-- Table structure for tickets
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created` datetime(0) NOT NULL,
  `cid` int(11) NOT NULL,
  `status` enum('Solved','Processing','Waiting') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `section` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tickets
-- ----------------------------

-- ----------------------------
-- Table structure for tickets_th
-- ----------------------------
DROP TABLE IF EXISTS `tickets_th`;
CREATE TABLE `tickets_th`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tid` int(11) NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `cdate` datetime(0) NOT NULL,
  `attach` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tid`(`tid`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE,
  INDEX `eid`(`eid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tickets_th
-- ----------------------------

-- ----------------------------
-- Table structure for todolist
-- ----------------------------
DROP TABLE IF EXISTS `todolist`;
CREATE TABLE `todolist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tdate` date NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` enum('Due','Done','Progress') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Due',
  `start` date NOT NULL,
  `duedate` date NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `eid` int(11) NOT NULL,
  `aid` int(11) NOT NULL,
  `related` int(11) NOT NULL,
  `priority` enum('Low','Medium','High','Urgent') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rid` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of todolist
-- ----------------------------

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acid` int(11) NOT NULL,
  `account` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` enum('Income','Expense','Transfer') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cat` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `debit` decimal(16, 2) NULL DEFAULT 0.00,
  `credit` decimal(16, 2) NULL DEFAULT 0.00,
  `payer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `payerid` int(11) NOT NULL DEFAULT 0,
  `method` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` date NOT NULL,
  `tid` int(11) NOT NULL DEFAULT 0,
  `eid` int(11) NOT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ext` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (5, 5, 'KAS BESAR', 'Income', 'Sales', 0.00, 8400000.00, 'PT. MERDEKA', 1, 'Cash', '2021-10-16', 1001, 14, 'Payment for invoice #1001', 0);

-- ----------------------------
-- Table structure for transactions_cat
-- ----------------------------
DROP TABLE IF EXISTS `transactions_cat`;
CREATE TABLE `transactions_cat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of transactions_cat
-- ----------------------------
INSERT INTO `transactions_cat` VALUES (1, 'Sales Order');
INSERT INTO `transactions_cat` VALUES (2, 'Pembelian');
INSERT INTO `transactions_cat` VALUES (3, 'Penjualan Tunai');

-- ----------------------------
-- Table structure for univarsal_api
-- ----------------------------
DROP TABLE IF EXISTS `univarsal_api`;
CREATE TABLE `univarsal_api`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `key1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `key2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `method` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `active` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of univarsal_api
-- ----------------------------
INSERT INTO `univarsal_api` VALUES (1, 'Goo.gl URL Shortner', 'yourkey', '0', '0', '0', '0', 0);
INSERT INTO `univarsal_api` VALUES (2, 'Twilio SMS API', 'ac', 'key', '+11234567', '0', '0', 1);
INSERT INTO `univarsal_api` VALUES (3, 'Company Support', '1', '1', 'support@gmail.com', NULL, '<p>Your footer</p>', 1);
INSERT INTO `univarsal_api` VALUES (4, 'Currency', ',', '.', '0', 'l', NULL, NULL);
INSERT INTO `univarsal_api` VALUES (5, 'Exchange', 'key1', 'key2', 'USD', NULL, NULL, 1);
INSERT INTO `univarsal_api` VALUES (6, 'New Invoice Notification', '[{Company}] Invoice #{BillNumber} Generated', NULL, NULL, NULL, 'Dear Client,\r\nWe are contacting you in regard to a payment received for invoice # {BillNumber} that has been created on your account. You may find the invoice with below link.\r\n\r\nView Invoice\r\n{URL}\r\n\r\nWe look forward to conducting future business with you.\r\n\r\nKind Regards,\r\nTeam\r\n{CompanyDetails}', NULL);
INSERT INTO `univarsal_api` VALUES (7, 'Invoice Payment Reminder', '[{Company}] Invoice #{BillNumber} Payment Reminder', NULL, NULL, NULL, '<p>Dear Client,</p><p>We are contacting you in regard to a payment reminder of invoice # {BillNumber} that has been created on your account. You may find the invoice with below link. Please pay the balance of {Amount} due by {DueDate}.</p><p>\r\n\r\n<b>View Invoice</b></p><p><span style=\"font-size: 1rem;\">{URL}\r\n</span></p><p><span style=\"font-size: 1rem;\">\r\nWe look forward to conducting future business with you.</span></p><p><span style=\"font-size: 1rem;\">\r\n\r\nKind Regards,\r\n</span></p><p><span style=\"font-size: 1rem;\">\r\nTeam\r\n</span></p><p><span style=\"font-size: 1rem;\">\r\n{CompanyDetails}</span></p>', NULL);
INSERT INTO `univarsal_api` VALUES (8, 'Invoice Refund Proceeded', '{Company} Invoice #{BillNumber} Refund Proceeded', NULL, NULL, NULL, '<p>Dear Client,</p><p>\r\nWe are contacting you in regard to a refund request processed for invoice # {BillNumber} that has been created on your account. You may find the invoice with below link. Please pay the balance of {Amount}  by {DueDate}.\r\n</p><p>\r\nView Invoice\r\n</p><p>{URL}\r\n</p><p>\r\nWe look forward to conducting future business with you.\r\n</p><p>\r\nKind Regards,\r\n</p><p>\r\nTeam\r\n\r\n{CompanyDetails}</p>', NULL);
INSERT INTO `univarsal_api` VALUES (9, 'Invoice payment Received', '{Company} Payment Received for Invoice #{BillNumber}', NULL, NULL, NULL, '<p>Dear Client,\r\n</p><p>We are contacting you in regard to a payment received for invoice  # {BillNumber} that has been created on your account. You can find the invoice with below link.\r\n</p><p>\r\nView Invoice</p><p>\r\n{URL}\r\n</p><p>\r\nWe look forward to conducting future business with you.\r\n</p><p>\r\nKind Regards,\r\n</p><p>\r\nTeam\r\n</p><p>\r\n{CompanyDetails}</p>', NULL);
INSERT INTO `univarsal_api` VALUES (10, 'Invoice Overdue Notice', '{Company} Invoice #{BillNumber} Generated for you', NULL, NULL, NULL, '<p>Dear Client,</p><p>\r\nWe are contacting you in regard to an Overdue Notice for invoice # {BillNumber} that has been created on your account. You may find the invoice with below link.\r\nPlease pay the balance of {Amount} due by {DueDate}.\r\n</p><p>View Invoice\r\n</p><p>{URL}\r\n</p><p>\r\nWe look forward to conducting future business with you.\r\n</p><p>\r\nKind Regards,\r\n</p><p>\r\nTeam</p><p>\r\n\r\n{CompanyDetails}</p>', NULL);
INSERT INTO `univarsal_api` VALUES (11, 'Quote Proposal', '{Company} Quote #{BillNumber} Generated for you', NULL, NULL, NULL, '<p>Dear Client,</p><p>\r\nWe are contacting you in regard to a new quote # {BillNumber} that has been created on your account. You may find the quote with below link.\r\n</p><p>\r\nView Invoice\r\n</p><p>{URL}\r\n</p><p>\r\nWe look forward to conducting future business with you.</p><p>\r\n\r\nKind Regards,</p><p>\r\n\r\nTeam</p><p>\r\n\r\n{CompanyDetails}</p>', NULL);
INSERT INTO `univarsal_api` VALUES (12, 'Purchase Order Request', '{Company} Purchase Order #{BillNumber} Requested', NULL, NULL, NULL, '<p>Dear Client,\r\n</p><p>We are contacting you in regard to a new purchase # {BillNumber} that has been requested on your account. You may find the order with below link. </p><p>\r\n\r\nView Invoice\r\n</p><p>{URL}</p><p>\r\n\r\nWe look forward to conducting future business with you.</p><p>\r\n\r\nKind Regards,\r\n</p><p>\r\nTeam</p><p>\r\n\r\n{CompanyDetails}</p>', NULL);
INSERT INTO `univarsal_api` VALUES (13, 'Stock Return Mail', '{Company} New purchase return # {BillNumber}', NULL, NULL, NULL, 'Dear Client,\r\n\r\nWe are contacting you in regard to a new purchase return # {BillNumber} that has been requested on your account. You may find the order with below link.\r\n\r\nView Invoice\r\n\r\n{URL}\r\n\r\nWe look forward to conducting future business with you.\r\n\r\nKind Regards,\r\n\r\nTeam\r\n\r\n{CompanyDetails}', NULL);
INSERT INTO `univarsal_api` VALUES (30, 'New Invoice Notification', NULL, NULL, NULL, NULL, 'Dear Customer, new invoice  # {BillNumber} generated. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (31, 'Invoice Payment Reminder', NULL, NULL, NULL, NULL, 'Dear Customer, Please make payment of invoice  # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (32, 'Invoice Refund Proceeded', NULL, NULL, NULL, NULL, 'Dear Customer, Refund generated of invoice # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (33, 'Invoice payment Received', NULL, NULL, NULL, NULL, 'Dear Customer, Payment received of invoice # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (34, 'Invoice Overdue Notice', NULL, NULL, NULL, NULL, 'Dear Customer, Dear Customer,Payment is overdue of invoice # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (35, 'Quote Proposal', NULL, NULL, NULL, NULL, 'Dear Customer, Dear Customer, a quote created for you # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (36, 'Purchase Order Request', NULL, NULL, NULL, NULL, 'Dear Customer, Dear, a purchased order for you # {BillNumber}. {URL} Regards', NULL);
INSERT INTO `univarsal_api` VALUES (51, 'QT#', 'PO#', 'REC#', 'SR#', 'TRN#', 'SRN#', 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `var_key` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `profile_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`users_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '1', NULL, 'active', '0', 'PT. MERDEKA', '$2y$10$RUp8d3RiDWms24y.6vnNF.QV4yDgwbvuUYwcgcIPWmsB2R9ABqfHe', 'merdeka@gmail.com', NULL, 'Member', 1);

SET FOREIGN_KEY_CHECKS = 1;
